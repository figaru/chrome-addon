var running = false;
var beat = {};
var heartBeats = [];
var conn;
var count = 0;

var tabNow;
var tabPrevious;

var token;
var tabTitle;
var tabDomain;
var tabUrl;
var timestamp;
var gmail;

if(running == false){
  chrome.browserAction.setBadgeBackgroundColor({ color: "#CC0000" });
      chrome.browserAction.setBadgeText({ text: "-" });
} else{
      chrome.browserAction.setBadgeBackgroundColor({ color: "#009933" });
      chrome.browserAction.setBadgeText({ text: ">" });}
    
chrome.tabs.onActivated.addListener(function(){
  tabPrevious = tabNow;
  tabNow = chrome.tabs.getSelected(null, function(tab) {})

  chrome.runtime.sendMessage({greeting: "beat"}, function(response) {
    });
});

chrome.tabs.onUpdated.addListener(function(){
  chrome.runtime.sendMessage({greeting: "beat"}, function(response) {
    });
});

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.greeting == "beat"){
      if(running == true){
          tabDomain = sender.tab.url.match(/^[\w-]+:\/*\[?([\w\.:-]+)\]?(?::\d+)?/)[1];
          tabTitle = sender.tab.title;
          tabUrl = sender.tab.url;
          timestamp = (new Date).getTime()

          beat.token = token;
          beat.title = tabTitle;
          beat.domain = tabDomain;
          beat.url = " "+ splitUrl(tabUrl).pathname + splitUrl(tabUrl).search;
          beat.timestamp = timestamp;

          sendResponse({farewell: "received"});
          sendHeartBeat(beat);
      }
      else{
        console.log("TeamOpz not running");
      }
    }
    if (request.greeting == "start"){
      running = true;
      token = request.token;
      chrome.browserAction.setBadgeBackgroundColor({ color: "#009933" });
      chrome.browserAction.setBadgeText({ text: ">" });
      sendResponse({farewell: "Started Logging"});
    }
    if (request.greeting == "stop"){
      running = false;
      chrome.browserAction.setBadgeBackgroundColor({ color: "#CC0000" });
      chrome.browserAction.setBadgeText({ text: "-" });
      sendResponse({farewell: "Stopped Logging"});
    }
    if (request.greeting == "state"){
      if(running == true)
      {
        sendResponse({farewell: "true"});
      } else {
        sendResponse({farewell: "false"});
      }
    }
    if (request.greeting == "gmail"){
      //gmailCheck();
      sendResponse({farewell: "Gmail Script"});
    }
  });

chrome.storage.local.get(/* String or Array */["token"], function(items){
            //  items = [ { "phasersTo": "awesome" } ]
            token = items.token;
            console.log(token);
});

function splitUrl(url){
    var match = url.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)(\/[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
        //protocol: match[1],   //"protocol": "http:",
        //host: match[2],       //"host": "example.com:3000",
        hostname: match[3],     //"hostname": "example.com",
        //port: match[4],       //"port": "3000",
        pathname: match[5],     //"pathname": "/pathname/",
        search: match[6]       //"search": "?search=test",
        //hash: match[7]        //"hash": "#hash"
    }
}

//test internet connection to update serve
function testConnection() {
        var url = "http://google.com";
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = function() { conn = true; }
        xmlhttp.onerror = function() { conn = false; }
        xmlhttp.open("GET",url,true);
        xmlhttp.send();
}

//update server with offline/unsuccessful beats
function updateServer(){
            testConnection();
            //if array is empty nothing is updated
            if(conn == true){
                console.log("Updating if needed.");
                for(var i = 0; i < heartBeats.length-1; i++){
                updateServerBeat(heartBeats[i]);
                //console.log(heartBeats[i]);
                }
            } else {
                console.log("No internet connection");
            }
}

//script to send array of offline/unsuccessful heartbeats to server
function updateServerBeat(heart) {
        var data = JSON.stringify(heart);// this is your data that you want to pass to the server (could be json)
        //next you would initiate a XMLHTTPRequest as following (could be more advanced):
        var url = "http://teamopz3.meteor.com/v1/browser/logs";//your url to the server side file that will receive the data.
        var http = new XMLHttpRequest();
        http.open("POST", url, true);

        //Send the proper header information along with the request
        //http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //http.setRequestHeader("Content-length", data.length);
        //http.setRequestHeader("Connection", "close");

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                count++;//count and how many objects have been uploaded to server from array
                console.log("HeartBeat Sent: ")
                console.log(http.responseText);//check if the data was received successfully.
                if(count == heartBeats.length-1)//compare to heartbeats.length-1 how many objects have been sent to server | if the same clear array(heartbeats)! 
                {
                    heartBeats = [];//clear array
                    console.log("Array as been cleared");//log the event
                    count = 0;//resent i to 0. = array[0]
                }
            }  
        }
        http.send(data);//send object to server
}

//save offline/unsuccessfully sent beats in array of objects
function saveBeatToArray(){
        console.log("No Internet Connection | Saved to localStorage");
                heartBeats[heartBeats.length] = ({
                    token: token,
                    title: tabTitle,
                    domain: tabDomain,
                    url: tabUrl,
                    timestamp: timestamp
                });
                console.log(heartBeats.toString());
}

//used to send the beat to the server - IF failed save beat in array
function sendHeartBeat(heart) {

        var data = JSON.stringify(heart);// Data to pass to the server
        var url = "http://teamopz3.meteor.com/v1/browser/logs";//your url to the server side file that will receive the data.
        var http = new XMLHttpRequest(); //next you would initiate a XMLHTTPRequest:
        http.open("POST", url, true); //what kind of request will we be sending

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                console.log("HeartBeat Sent: ")
                console.log(http.responseText);//check if the data was received successfully.
            }  
        }
        http.onerror = function() { saveBeatToArray();};//if failed to send heartbeat to server | Save to array
        http.send(data);//send object to server
}

var timerUpdate = setInterval(function() { 
   updateServer();
}, 30000);





// -------------------------------COMMENTED OUT----------------------------------------
/*chrome.windows.onRemoved.addListener(function(windowId){
  running = false;
  chrome.browserAction.setBadgeBackgroundColor({ color: "#CC0000" });
      chrome.browserAction.setBadgeText({ text: "-" });
  console.log("Browser exit!");
});*/

