// content.js
var didScroll = false;
var didClick = false;
var didKey = false;

//INterval check every 5 seconds for click, scroll and key events
setInterval(function() {
    if(didScroll || didClick || didKey) {
        didScroll = false;
        didClick = false;
        didKey = false;

        chrome.runtime.sendMessage({greeting: "beat"}, function(response) {
          console.log(response.farewell);
        });
    }
}, 10000);

//--------------Page OnScroll------------
window.onscroll = scrolled;

function scrolled() {
    didScroll = true;
}

//-------------MouseClick---------------
window.onclick = clicked;

function clicked(){
    didClick = true;
}

//-------------key pressed------------
window.onkeyup = keyed;

function keyed(){
    didKey = true;
}

window.onbeforeunload = function(){
  chrome.runtime.sendMessage({greeting: "beat"}, function(response) {
            console.log(response.farewell);
        });
};

window.onload = function(){
  chrome.runtime.sendMessage({greeting: "beat"}, function(response) {
            console.log(response.farewell);
        });
};