// When the user hits return, send the "text-entered"
// The message payload is the contents of the edit box.
var input = document.getElementById("input-token");
var start = document.getElementById("input-start");
var stop = document.getElementById("input-stop");
var more = document.getElementById("more");
var message = document.getElementById("message");
var token;

more.addEventListener("click", function(){
    chrome.tabs.create({url: chrome.extension.getURL('/settings/background.html')});
});

    
chrome.storage.local.get(/* String or Array */["token"], function(items){
            //  items = [ { "phasersTo": "awesome" } ]
            token = items.token;
            message.innerHTML = "<b>Your Token:</b> " + token;
        });

chrome.runtime.sendMessage({greeting: "state"}, function(response) {
            if(response.farewell == "true"){
                start.click();
            }
            else{
                console.log("Not logging");
            }
        });

start.addEventListener("click", function(){
    if(token != '')
    {
        chrome.runtime.sendMessage({greeting: "start"}, function(response) {
            console.log(response.farewell);
        });
        //chrome.runtime.sendMessage({token: text}, function(response) { });
    	start.style.visibility="hidden";
    	stop.style.visibility="visible";
    	start.style.display="none";
    	stop.style.display="block";
    }
    else {
        //there is not token in system
    	message.innerHTML = "Please insert your token in options to start logging.";
    }
});

stop.addEventListener("click", function(){
    chrome.runtime.sendMessage({greeting: "stop"}, function(response) {
            console.log(response.farewell);
        });
   	stop.style.visibility="hidden";
    start.style.visibility="visible";
    start.style.display="block";
    	stop.style.display="none";
});
